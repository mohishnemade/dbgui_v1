package controllers

import (
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/mohishnemade/dbgui_v1/api/auth"
	"bitbucket.org/mohishnemade/dbgui_v1/api/models"
	"bitbucket.org/mohishnemade/dbgui_v1/api/responses"
	"bitbucket.org/mohishnemade/dbgui_v1/api/utils/formaterror"
	"golang.org/x/crypto/bcrypt"
)

func (server *Server) Register(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")

	tokenID, err := auth.ExtractTokenID(r)

	if err != nil {
		//responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
		fmt.Fprint(w, tokenID)

		http.ServeFile(w, r, "./page-register.html")
		return
	}
	http.Redirect(w, r, "/getData", 301)

}

func (server *Server) Logout(w http.ResponseWriter, r *http.Request) {
	expire := time.Now().Add(30 * time.Minute)
	ac, err := r.Cookie("token")
	fmt.Printf(ac.Value)

	if err != nil {

	}
	ac.Value = ""
	cookie := http.Cookie{
		Name:    "token",
		Value:   "",
		Expires: expire,
	}

	http.SetCookie(w, &cookie)
	ac, err = r.Cookie("token")
	fmt.Printf(ac.Value)
	fmt.Println("-------------------------")

	//	w.Header().Set("Content-Type", "text/html; charset=utf-8")

	http.Redirect(w, r, "/", 301)
}

func (server *Server) Login(w http.ResponseWriter, r *http.Request) {

	user := models.User{
		//Nickname: r.FormValue("name"),
		Email:    r.FormValue("email"),
		Password: r.FormValue("password"),
	}
	/*	body, err := ioutil.ReadAll(details)
		if err != nil {
			responses.ERROR(w, http.StatusUnprocessableEntity, err)
			return
		}
		user := models.User{}
		err = json.Unmarshal(body, &user)
		if err != nil {
			responses.ERROR(w, http.StatusUnprocessableEntity, err)
			return
		}
	*/
	user.Prepare()

	err := user.Validate("login")
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	token, err := server.SignIn(user.Email, user.Password)
	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		responses.ERROR(w, http.StatusUnprocessableEntity, formattedError)
		return
	}
	//auth.addCookie(w, token, user.uid, 30*time.Minute)
	expire := time.Now().Add(30 * time.Minute)
	cookie := http.Cookie{
		Name:    "token",
		Value:   token,
		Expires: expire,
	}
	http.SetCookie(w, &cookie)
	w.Header().Set("Content-Type", "text/html; charset=utf-8")

	http.Redirect(w, r, "/getData", 301)
	//responses.JSON(w, http.StatusOK, token)

}

func (server *Server) SignIn(email, password string) (string, error) {

	var err error

	user := models.User{}

	err = server.DB.Debug().Model(models.User{}).Where("email = ?", email).Take(&user).Error
	if err != nil {
		return "", err
	}
	err = models.VerifyPassword(user.Password, password)
	if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
		return "", err
	}
	return auth.CreateToken(user.ID)
}
