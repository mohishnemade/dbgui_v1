package controllers

import (
	"fmt"
	"net/http"

	"bitbucket.org/mohishnemade/dbgui_v1/api/middlewares"
	"github.com/jasonlvhit/gocron"
	"github.com/jinzhu/gorm"
)

func myTask() {
	fmt.Println("This task running fine")
}

//ExecuteCronJob is ...
func ExecuteCronJob(DB *gorm.DB) {
	gocron.Every(1).Hours().Do(myTask)
	<-gocron.Start()
}

func (s *Server) initializeRoutes() {
	go ExecuteCronJob(s.DB)

	//	SetMiddlewareJSON: This will format all responses to Html
	//  SetMiddlewareAuthentication: This will check for the validity of the authentication token provided.
	// Home Route

	//	http.Handle("/index.html", http.StripPrefix("/assets/", http.FileServer(http.Dir("./api/templates"))))

	fs := http.FileServer(http.Dir("./api/templates"))
	fassets := http.FileServer(http.Dir("./assets"))
	fassets1 := http.FileServer(http.Dir("./images"))
	//fassets2 := http.FileServer(http.Dir("./api/templates/assets/"))

	s.Router.Handle("/index.html", fs)
	s.Router.Handle("/templates/", http.StripPrefix("/templates/", fassets1))
	//s.Router.Handle("/assets/", fassets)
	//	s.Router.Handle("/assets/", http.StripPrefix("/assets/", fassets))
	s.Router.PathPrefix("/assets/").Handler(http.StripPrefix("/assets/", fassets))
	s.Router.PathPrefix("/images/").Handler(http.StripPrefix("/images/", fassets1))

	s.Router.HandleFunc("/abc", middlewares.SetMiddlewareJSON(s.Homenew)).Methods("GET")

	s.Router.HandleFunc("/", middlewares.SetMiddlewareJSON(middlewares.SetMiddlewareAuthentication(s.Home))).Methods("GET")
	//s.Router.HandleFunc("/", s.Home).Methods("GET")

	s.Router.HandleFunc("/register", middlewares.SetMiddlewareJSON(s.Register)).Methods("GET")

	// Login Route
	s.Router.HandleFunc("/login", middlewares.SetMiddlewareJSON(s.Login)).Methods("POST")

	// Logout the current user
	s.Router.HandleFunc("/logout", middlewares.SetMiddlewareJSON(s.Logout)).Methods("GET")

	//Users routes
	// Will create a user with details provided in the post request
	s.Router.HandleFunc("/users", middlewares.SetMiddlewareJSON(s.CreateUser)).Methods("POST")

	// will get all the users
	s.Router.HandleFunc("/users", middlewares.SetMiddlewareJSON(s.GetUsers)).Methods("GET")

	//get the user with particular Id
	s.Router.HandleFunc("/users/{id}", middlewares.SetMiddlewareJSON(s.GetUser)).Methods("GET")
	// Update the user
	s.Router.HandleFunc("/users/{id}", middlewares.SetMiddlewareJSON(middlewares.SetMiddlewareAuthentication(s.UpdateUser))).Methods("PUT")
	s.Router.HandleFunc("/users/{id}", middlewares.SetMiddlewareAuthentication(s.DeleteUser)).Methods("DELETE")

	//DB inventory
	//Get data from rds table
	s.Router.HandleFunc("/getData", middlewares.SetMiddlewareJSON(middlewares.SetMiddlewareAuthentication(s.GetData))).Methods("GET")
	//Get data from pmm table
	s.Router.HandleFunc("/getPmmData", middlewares.SetMiddlewareJSON(middlewares.SetMiddlewareAuthentication(s.GetPmmData))).Methods("GET")
	//Get data from temp table
	s.Router.HandleFunc("/getTempData", middlewares.SetMiddlewareJSON(middlewares.SetMiddlewareAuthentication(s.GetTempData))).Methods("GET")

	//Pull data from awsrds
	s.Router.HandleFunc("/pullRdsData", s.pullRdsData).Methods("POST")
	//	s.Router.HandleFunc("/pullPmmData", middlewares.SetMiddlewareJSON(s.PullPmmData)).Methods("GET")
	s.Router.HandleFunc("/pullTempData", middlewares.SetMiddlewareJSON(middlewares.SetMiddlewareAuthentication(s.PullTempData))).Methods("GET")

}
