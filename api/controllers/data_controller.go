package controllers

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"strconv"

	"bitbucket.org/mohishnemade/dbgui_v1/api/models"
	"bitbucket.org/mohishnemade/dbgui_v1/api/responses"
	"bitbucket.org/mohishnemade/dbgui_v1/api/utils/formaterror"
	"github.com/gorilla/mux"
)

func (server *Server) pullRdsData(w http.ResponseWriter, r *http.Request) {

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
	}
	rdsdata := models.Rdsdata{}
	err = json.Unmarshal(body, &rdsdata)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	//	rdsdata.Prepare()
	//  err = rdsdata.Validate("")
	//	if err != nil {
	//		responses.ERROR(w, http.StatusUnprocessableEntity, err)
	//		return
	//    }
	userCreated, err := rdsdata.SaveData(server.DB)

	if err != nil {

		formattedError := formaterror.FormatError(err.Error())

		responses.ERROR(w, http.StatusInternalServerError, formattedError)
		return
	}
	w.Header().Set("Location", fmt.Sprintf("%s%s/%d", r.Host, r.RequestURI, userCreated.ID))
	responses.JSON(w, http.StatusCreated, userCreated)
//	http.Redirect(w, r, "/getData", 301)

}

func (server *Server) GetData(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	tmpl, err := template.ParseFiles("./index.html")
	if err != nil {
		http.ServeFile(w, r, "./page-login.html")

	}

	data := models.Rdsdata{}
	datas, err := data.FindAllData(server.DB)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}

	//responses.JSON(w, http.StatusOK, datas)
	fmt.Println("----9999---")

	tmpl.Execute(w, datas)
	//http.ServeFile(w, r, "./page-login.html")

}

func (server *Server) GetPmmData(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	tmpl, err := template.ParseFiles("./pmm-db.html")
	if err != nil {
		http.ServeFile(w, r, "./page-login.html")

	}

	data := models.Pmmdata{}
	datas, err := data.FindAllPmmData(server.DB)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	//responses.JSON(w, http.StatusOK, datas)
	tmpl.Execute(w, datas)
}

func (server *Server) GetTempData(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	tmpl, err := template.ParseFiles("./temp-db.html")
	if err != nil {
		http.ServeFile(w, r, "./page-login.html")

	}
	data := models.Tempdata{}
	datas, err := data.SimplyGetTempData(server.DB)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	tmpl.Execute(w, datas)

	//responses.JSON(w, http.StatusOK, datas)
}

func (server *Server) PullTempData(w http.ResponseWriter, r *http.Request) {

	data := models.Tempdata{}
	datas, err := data.FindAllTempData(server.DB)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	//	responses.JSON(w, http.StatusOK, datas)
	_ = datas
	http.Redirect(w, r, "/getData", 301)

}

/*
func (server *Server) PullPmmData(w http.ResponseWriter, r *http.Request) {

	data := models.Tempdata{}
	datas, err := data.FindAllPmmData(server.DB)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	responses.JSON(w, http.StatusOK, datas)
}
*/
func (server *Server) GetDat(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	uid, err := strconv.ParseUint(vars["id"], 10, 32)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	user := models.User{}
	userGotten, err := user.FindUserByID(server.DB, uint32(uid))
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	responses.JSON(w, http.StatusOK, userGotten)
}
