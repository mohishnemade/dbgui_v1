package controllers

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"

	"bitbucket.org/mohishnemade/dbgui_v1/api/models"
	_ "github.com/jinzhu/gorm/dialects/mysql"    //mysql database driver
	_ "github.com/jinzhu/gorm/dialects/postgres" //postgres database driver
	_ "github.com/jinzhu/gorm/dialects/sqlite"   // sqlite database driver
)

// Server is ...
type Server struct {
	DB     *gorm.DB
	Router *mux.Router
}

// this initializes the routes by connecting the particular database first
func (server *Server) Initialize(Dbdriver, DbUser, DbPassword, DbPort, DbHost, DbName string) {

	var err error
	if Dbdriver == "mysql" {
		DBURL := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", DbUser, DbPassword, DbHost, DbPort, DbName)
		server.DB, err = gorm.Open(Dbdriver, DBURL)
		if err != nil {
			fmt.Printf("Cannot connect tso %s database", Dbdriver)
			log.Fatal("This is the error:", err)
		} else {
			fmt.Printf("We are connected to the %s database", Dbdriver)
		}
	}

	server.DB.Debug().AutoMigrate(&models.User{}) //database migration

	server.Router = mux.NewRouter()

	server.initializeRoutes()
}

// Run is ..
func (server *Server) Run(addr string) {
	fmt.Println("Listening to port 8080")
	//	http.Handle("assets", http.FileServer(http.Dir("./assets")))
	//	http.Handle("/index.html", http.StripPrefix("/assets/", http.FileServer(http.Dir("./api/templates"))))
	http.Handle("/index.html", http.FileServer(http.Dir("./")))

	log.Fatal(http.ListenAndServe(addr, server.Router))
}
