package api

import (
	"fmt"
	"log"
	"os"
	"bitbucket.org/mohishnemade/dbgui_v1/api/seed"

	"bitbucket.org/mohishnemade/dbgui_v1/api/controllers"
	"bitbucket.org/mohishnemade/dbgui_v1/api/models"

	"github.com/joho/godotenv"
)

var server = controllers.Server{}

func init() {
	// loads values from .env into the system
	if err := godotenv.Load(); err != nil {
		log.Print("sad .env file found")
	}
}

func Run() {

	var err error
	err = godotenv.Load()
	if err != nil {
		log.Fatalf("Error getting env, %v", err)
	} else {
		fmt.Println("We are getting the env values")
	}

	server.Initialize(os.Getenv("DB_DRIVER"), os.Getenv("DB_USER"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_PORT"), os.Getenv("DB_HOST"), os.Getenv("DB_NAME"))

	table_check := server.DB.Debug().Model(&models.Rdsdata{}).Limit(100).First(&models.Rdsdata{}).Error
    if table_check == nil {
        fmt.Println("Working fine ")
    } else {
		seed.Load(server.DB)
    }

	server.Run(":8080")

}
