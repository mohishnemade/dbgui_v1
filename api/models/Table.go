package models

import (
	_ "errors"
	_ "log"
	_ "strings"
"fmt"
	_ "github.com/badoux/checkmail"
	"github.com/jinzhu/gorm"
	_ "golang.org/x/crypto/bcrypt"
)

type Result struct {
    Field    string
    Type     string
    Null     string
    Key      string
    Default  string
    Extra    string
}

func (u *Result) compareTables(db *gorm.DB,tableName string) (*Result, error) {

	var err error
	datas := Result{}
//	err = db.Debug().Model(&Rdsdata{}).Limit(100).Find(&datas).Error

	var name string
	name = "DESCRIBE "+tableName
	db.Raw(name).Scan(&datas)

	if err != nil {
		return &Result{}, err
	}
	return &datas, err
}

func (u *Result) compareColumns(db *gorm.DB,tableName string) (*Result, error) {

	var err error
	datas := Result{}
//	err = db.Debug().Model(&Rdsdata{}).Limit(100).Find(&datas).Error

	var name string
	name = "DESCRIBE "+tableName
	db.Raw(name).Scan(&datas)

	if err != nil {
		return &Result{}, err
	}
	return &datas, err
}

func (u *Result) compareDataTypeChange(db *gorm.DB,tableName string) (*[]Result, error) {

	var err error
	datas := []Result{}
	csvdatas := []Result{}

//	err = db.Debug().Model(&Rdsdata{}).Limit(100).Find(&datas).Error

	var name string
	name = "DESCRIBE "+tableName
	db.Raw(name).Scan(&datas)

	for i, _ := range datas {
		
		if (datas[i].Type != csvdatas[i].Type){
			fmt.Println("Table data type altered")
		}

	}

	if err != nil {
		return &[]Result{}, err
	}
	return &datas, err
}