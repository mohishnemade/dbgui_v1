package services

import (
	"fmt"

	"github.com/jasonlvhit/gocron"
	"github.com/jinzhu/gorm"
)

func myTask() {
	fmt.Println("This task will run periodically")

	//var err error

}

func ExecuteCronJob(DB *gorm.DB) {
	gocron.Every(1).Second().Do(myTask)
	<-gocron.Start()
}
